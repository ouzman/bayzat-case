# bayzat-case
This program calculates how many different ways you can remove the balls from the bag one ball or two balls at a time.

### Test
```
./gradlew test
```

### Run
```
./gradlew run --args="5"
```
or
```
./gradlew installDist
./build/install/bayzat-case/bin/bayzat-case 5
```
