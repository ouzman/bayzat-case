package com.oguzhanuzman.bayzatcase;

import org.junit.Assert;
import org.junit.Test;

public class BallSelectionTest {
    @Test
    public void shouldFind3WaysWhen3Balls() {
        BallSelection ballSelection = new BallSelection(3);

        long actual = ballSelection.findNumberOfDifferentWays();

        Assert.assertEquals(3, actual);
    }

    @Test
    public void shouldFind8WaysWhen3Balls() {
        BallSelection ballSelection = new BallSelection(5);

        long actual = ballSelection.findNumberOfDifferentWays();

        Assert.assertEquals(8, actual);
    }

    @Test
    public void shouldThrowExceptionWhenNegativeNumberOfBalls() {
        Assert.assertThrows(IllegalArgumentException.class, () -> new BallSelection(-1));
    }
}
