package com.oguzhanuzman.bayzatcase;

public class BallSelection {
    private final int numberOfBalls;

    public BallSelection(int numberOfBalls) {
        if (numberOfBalls < 1) {
            throw new IllegalArgumentException("Number of balls should be greater than or equal to 1.");
        }

        this.numberOfBalls = numberOfBalls;
    }

    public long findNumberOfDifferentWays() {
        return findNumberOfDifferentWays(this.numberOfBalls);
    }

    private long findNumberOfDifferentWays(int numberOfBalls) {
        switch (numberOfBalls) {
            case 1:
                return 1L;
            case 2:
                return 2L;
            default:
                return findNumberOfDifferentWays(numberOfBalls - 1) + findNumberOfDifferentWays(numberOfBalls - 2);
        }
    }
}
