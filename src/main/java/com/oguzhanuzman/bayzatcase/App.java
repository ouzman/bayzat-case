package com.oguzhanuzman.bayzatcase;

import picocli.CommandLine;

public class App {
    static class BayzatCase implements Runnable {
        @CommandLine.Parameters(paramLabel = "INPUT", description = "number of balls in the bag")
        private int numberOfBalls;

        @Override
        public void run() {
            long numberOfDifferentWays = new BallSelection(this.numberOfBalls).findNumberOfDifferentWays();
            System.out.printf("n: %d, result: %d\n", this.numberOfBalls, numberOfDifferentWays);
        }
    }

    public static void main(String[] args) {
        new CommandLine(new BayzatCase()).execute(args);
    }
}
